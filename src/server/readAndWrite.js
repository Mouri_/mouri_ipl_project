/**
 * Importing file systems module to read matches and deliverie csv files
 * @constant
 * @type {function}
 */
const fs = require("fs");

/**
 * Importing path module to convert relative path to absolute path
 * @constant
 * @type {function}
 */
const path = require("path");

function writeFileAsJson(dataToWrite, relativePath) {


    const absolutePath = path.resolve(relativePath)

    const absoluteDirPath = absolutePath.substring(0, absolutePath.lastIndexOf("/"));

    const dataToWriteAsString = JSON.stringify(dataToWrite);

    /** Checks if the output directory exists, if not creates one */
    if (!fs.existsSync(absoluteDirPath)) {
        fs.mkdirSync(absoluteDirPath);
    }

    fs.writeFile(absolutePath, dataToWriteAsString, errorHandler);
}

/**
 * Reads file using fs (synchronously) and returns its value
 * @param {string} relativePath -Path of the file
 * @returns {string}
 */
function read(relativePath) {

    return fs.readFileSync(path.resolve(relativePath), {
        encoding: "utf8",
        flag: "r",
    });
}

/**
 * Handles the error, if any
 * @param {error} error 
 */
function errorHandler(error) {
    if (error != null) {
        console.error(error);
    }
}

module.exports = {
    read,
    writeFileAsJson
}