/**
 * Importing path module to convert relative path to absolute path
 * @constant
 * @type {function}
 */
const path = require("path");

/**
 * Importing ipl module to run all functions
 * @constant
 * @type {function}
 * @param {module name}
 */
const ipl = require(path.resolve('./ipl'));

/**
 * Importing readAndWrite module to read and write the json files
 * @constant
 * @type {function}
 * @param {module name}
 */
const readAndWrite = require(path.resolve('./readAndWrite'));

const DELIVERIES_JSON_PATH = "../data/deliveries.json";

const MATCHES_JSON_PATH = "../data/matches.json";

/**
 * Used to store match data in array format
 * @constant
 * @type {Array<Array>}
 */
const MATCHES_ARRAY = JSON.parse(readAndWrite.read(MATCHES_JSON_PATH));

/**
 * Used to store deliveries data in array format
 * @constant
 * @type {Array<Array>}
 */
const DELIVERIES_ARRAY = JSON.parse(readAndWrite.read(DELIVERIES_JSON_PATH));

/** Freezing the arrays to avoid accidental modifications */
Object.freeze(MATCHES_ARRAY);
Object.freeze(DELIVERIES_ARRAY);

/**
 * Imported ipl.js functions, refer to each function for documentation
 */

/** Processes the matches array to find the number of matches played per year */
readAndWrite.writeFileAsJson(ipl.matchesPerYear(MATCHES_ARRAY), '../public/output/matchesPerYear.json');

/** Processes the matches array to find the number of matches each team won every year */
readAndWrite.writeFileAsJson(ipl.matchesWonPerYear(MATCHES_ARRAY), '../public/output/matchesWonPerYear.json');

/**
 * Processes the matches and deliveries array to find the extra runs conceded per team in the year 2016
 * Year default 2016, pass (extraRunsYear :)
 */
readAndWrite.writeFileAsJson(ipl.extraRuns({
    matchesArray: MATCHES_ARRAY,
    deliveriesArray: DELIVERIES_ARRAY
}), '../public/output/extraRuns.json');

/** 
 * Processes the matches and deliveries array to find the top 10 economical bowlers in the year 2015
 * Year default 2015, list size default 10, pass (year, listSize)
 */
readAndWrite.writeFileAsJson(ipl.topEconomicalBowlers({
    matchesArray: MATCHES_ARRAY,
    deliveriesArray: DELIVERIES_ARRAY
}), '../public/output/topEconomicalBowlers.json');

/** Processes the matches array to find the number of times each team won the toss and also won the match */
readAndWrite.writeFileAsJson(ipl.wonTheTossAndWonTheMatch(MATCHES_ARRAY), '../public/output/wonTheTossAndWonTheMatch.json');

/** 
 * Processes the matches array to find the player(s) who has won the highest number of Player of the Match awards for each season
 */
readAndWrite.writeFileAsJson(ipl.highestNumberOfPlayerOfTheMatch(MATCHES_ARRAY), '../public/output/highestNumberOfPlayerOfTheMatch.json');

/**
 * Processes the matches array to find the strike rate of a batsman for each season
 * Enter the name (playerName :) or leave it empty for full list
 */
readAndWrite.writeFileAsJson(ipl.strikeRateOfBatsmen({
    matchesArray: MATCHES_ARRAY,
    deliveriesArray: DELIVERIES_ARRAY
}), '../public/output/strikeRateOfBatsmen.json');

/** Processes the matches array to find the highest number of times one player has been dismissed by another player */
readAndWrite.writeFileAsJson(ipl.highestNumberOfDismissals(DELIVERIES_ARRAY), '../public/output/highestNumberOfDismissals.json');

/** Processes the matches array to find the bowler with the best economy in super overs */
readAndWrite.writeFileAsJson(ipl.bestSuperOverEconomyBowler(DELIVERIES_ARRAY), '../public/output/bestSuperOverEconomyBowler.json');