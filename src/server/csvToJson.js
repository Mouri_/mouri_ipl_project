/**
 * Importing readAndWrite module to write the json files
 * @constant
 * @type {function}
 * @param {module name}
 */
const readAndWrite = require('./readAndWrite')

/**
 * Importing path module to convert relative path to absolute path
 * @constant
 * @type {function}
 */
const path = require("path");

const DELIVERIES_CSV_ABS_PATH = path.resolve("../data/csv/deliveries.csv");

const MATCHES_CSV_ABS_PATH = path.resolve("../data/csv/matches.csv")

const DELIVERIES_JSON_PATH = "../data/deliveries.json"

const MATCHES_JSON_PATH = "../data/matches.json"

csvtojson().fromFile(DELIVERIES_CSV_ABS_PATH).then(data => {
    readAndWrite.writeFileAsJson(DELIVERIES_JSON_PATH, data)
}).catch(helperFunctions.errorHandler)

csvtojson().fromFile(MATCHES_CSV_ABS_PATH).then(data => {
    readAndWrite.writeFileAsJson(MATCHES_JSON_PATH, data)
}).catch(helperFunctions.errorHandler)

/**
 * Handles the error, if any
 * @param {error} error 
 */
function errorHandler(error) {
    if (error != null) {
        console.error(error);
    }
}