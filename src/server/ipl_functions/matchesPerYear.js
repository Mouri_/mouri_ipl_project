/**
 * Processes the matches array to find the number of matches played per year
 * @param {Array} matchesArray 
 * @returns {Object}
 */
function matchesPerYear(matchesArray) {

    /** Loops through matches array and updates the accumulator (object) with the number of matches per year */
    return matchesArray.reduce((accumulator, arr) => {
        let year = arr.season;
        if (accumulator.hasOwnProperty(year)) {
            accumulator[year]++;
        } else {
            accumulator[year] = 1;
        }
        return accumulator;
    }, {});
}

module.exports = matchesPerYear;