/**
 * Processes the matches array to find the player(s) who has won the highest number of Player of the Match awards for each season
 * @param {Array} matchesArray
 * @returns {Object}
 */
function highestNumberOfPlayerOfTheMatch(matchesArray) {

    /**
     * Loops through matches array and updates the accumulator (object) with the number of times each player won player of the match
     * @constant
     * @type {object}
     */
    const playerOfTheMatchCount = matchesArray.reduce((accumulator, arr) => {
        const playerOfTheMatch = arr.player_of_match;
        if (accumulator.hasOwnProperty(playerOfTheMatch)) {
            accumulator[playerOfTheMatch] += 1;
        } else {
            accumulator[playerOfTheMatch] = 1;
        }
        return accumulator;
    }, {});

    /**
     * Array to store the player(s) who won the maximum number of player of the match
     * @let
     * @type {Array<Array>}
     */
    let maxWinner = [
        ["", 0]
    ];

    /** Loops through the object and store the player(s) who won maximum number of player of the match in maxWinner array */
    for (const key in playerOfTheMatchCount) {
        const count = playerOfTheMatchCount[key];
        if (count > maxWinner[0][1]) {
            maxWinner = [
                [key, count]
            ];
        } else if (count == maxWinner[0][1]) {
            maxWinner.push([key, count]);
        }
    }

    /** Lopps through maxWinners array to add all entries to object and return it */
    return maxWinner.reduce((accumultor, value) => {
        accumultor[value[0]] = value[1];
        return accumultor;
    }, {});
}

module.exports = highestNumberOfPlayerOfTheMatch;