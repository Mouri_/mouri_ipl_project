/**
 * Processes the matches array to find the number of matches each team won every year
 * @param {Array} matchesArray 
 * @returns {Object}
 */
function matchesWonPerYear(matchesArray) {

    /** Loops through matches array and updates the accumulator (object) with matches won per year and returns it */
    return matchesArray.reduce((accumulator, arr) => {
        let winner = arr.winner;
        let year = arr.season;
        if (accumulator.hasOwnProperty(winner)) {
            if (accumulator[winner].hasOwnProperty(year)) {
                accumulator[winner][year]++;
            } else {
                accumulator[winner][year] = 1;
            }
        } else {
            accumulator[winner] = {
                [year]: 1
            };
        }
        return accumulator;
    }, {});
}

module.exports = matchesWonPerYear;