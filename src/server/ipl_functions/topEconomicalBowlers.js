/**
 * Processes the matches and deliveries array to find the top 10 economical bowlers in the year 2015
 * @param {Array} matchesArray 
 * @param {Array} deliveriesArray 
 * @param {Array} economicalBowlersYear 
 * @param {number} listSize 
 * @returns {Object}
 */
function topEconomicalBowlers({
    matchesArray,
    deliveriesArray,
    economicalBowlersYear = 2015,
    listSize = 10
}) {

    /** Gets the ID range */
    const fromAndToID = findMatchID(economicalBowlersYear, matchesArray);

    /** Extracting values from fromAndToID object */
    const fromID = fromAndToID.fromID;
    const toID = fromAndToID.toID;

    /**
     * Loops through deliveries array and updates the accumulator (object) with runs each bowler allows and number of balls (excluding bye and leg-bye runs, and no balls and wide balls)
     * @constant
     * @type {object}
     */
    const result = deliveriesArray.reduce((accumulator, arr) => {

        /** ID of the current array (ball) */
        const ID = arr.match_id;

        /** Checks if the ball is in the range of IDs of the given year */
        if (ID >= fromID && ID <= toID) {
            const bowler = arr.bowler;
            const byeAndLegByeRuns = parseInt(arr.bye_runs) + parseInt(arr.legbye_runs);
            const runs = parseInt(arr.total_runs) - byeAndLegByeRuns;
            const notAddedToOver = (arr.noball_runs || arr.wide_runs) != '0' ? 1 : 0;

            if (accumulator.hasOwnProperty(bowler)) {
                accumulator[bowler].runs_conceded += runs;
                accumulator[bowler].no_of_balls += 1 - notAddedToOver;
            } else {
                accumulator[bowler] = {};
                accumulator[bowler].runs_conceded = runs;
                accumulator[bowler].no_of_balls = 1 - notAddedToOver;
            }
        }
        return accumulator;
    }, {});

    /**
     * Array to store calculated economy and bowler name, for sorting purposes
     * @let
     * @type {Array}
     */
    let resultArray = [];

    /** Calculates economy for each bowler and pushes it into resultArray */
    for (const key in result) {
        const economy = (result[key].runs_conceded / (result[key].no_of_balls / 6)).toFixed(2);
        resultArray.push([key, economy]);
    }

    /** Sorting in accending order */
    resultArray.sort((a, b) => a[1] - b[1]);

    /** Trims the array to required size and returns it */
    return resultArray.slice(0, listSize).reduce((accumulator, value) => {
        accumulator[value[0]] = value[1];
        return accumulator
    }, {});
}

/**
 * Takes in a year and finds its ID range and returns it
 * @param {number} matchIdYear 
 * @param {Array} matchesArray
 * @returns {object}
 */
function findMatchID(matchIdYear, matchesArray) {

    let fromID = Number.MAX_VALUE;
    let toID = 0;

    /** Loops through matches array to find the ID range */
    matchesArray.forEach(arr => {
        let ID = parseInt(arr.id);
        let year = parseInt(arr.season);
        if (year == matchIdYear) {
            fromID = Math.min(fromID, ID);
            toID = Math.max(toID, ID);
        }
    });

    return {
        fromID: fromID,
        toID: toID,
    };
}

module.exports = topEconomicalBowlers;