/**
 * Processes the matches array to find the bowler with the best economy in super overs
 * @param {Array} deliveriesArray 
 * @returns {Object}
 */
function bestSuperOverEconomyBowler(deliveriesArray) {

    /**
     * Loops through deliveries array and updates the accumulator (object) with runs each bowler allows and number of balls (excluding bye and leg-bye runs, and no balls and wide balls)
     * @constant
     * @type {object}
     */
    let result = deliveriesArray.reduce((accumulator, arr) => {
        if (arr.is_super_over != 0) {
            const bowler = arr.bowler;
            const byeAndLegByeRuns = parseInt(arr.bye_runs) + parseInt(arr.legbye_runs);
            const runs = parseInt(arr.total_runs) - byeAndLegByeRuns;
            const notAddedToOver = (arr.noball_runs || arr.wide_runs) != '0' ? 1 : 0;

            if (accumulator.hasOwnProperty(bowler)) {
                accumulator[bowler].runs_conceded += runs;
                accumulator[bowler].no_of_balls += 1 - notAddedToOver;
            } else {
                accumulator[bowler] = {};
                accumulator[bowler].runs_conceded = runs;
                accumulator[bowler].no_of_balls = 1 - notAddedToOver;
            }
        }
        return accumulator;
    }, {});

    /**
     * Array to store calculated economy and bowler name, for sorting purposes
     * @let
     * @type {Array}
     */
    let resultArray = [];

    /** Calculates economy for each bowler and pushes it into resultArray */
    for (const key in result) {
        const economy = (result[key].runs_conceded / (result[key].no_of_balls / 6)).toFixed(2);
        resultArray.push([key, economy]);
    }

    /** Sorting in accending order */
    resultArray.sort((a, b) => a[1] - b[1]);

    /**
     * Adding the top economy bowler (first array element) to an object and returning it
     * @type {object} 
     */
    return {
        [resultArray[0][0]]: resultArray[0][1]
    };
}

module.exports = bestSuperOverEconomyBowler;