/**
 * Processes the matches array to find the highest number of times one player has been dismissed by another player
 * @param {Array} deliveriesArray 
 * @returns {Object}
 */
function highestNumberOfDismissals(deliveriesArray) {

    /**
     * Loops through deliveries array and updates the accumulator (object) with the number of times each player got dismissed by other players
     * @constant
     * @type {object}
     */
    const result = deliveriesArray.reduce((accumulator, arr) => {
        const dismissedPlayer = arr.player_dismissed;
        if (dismissedPlayer == "") {
            return accumulator;
        }
        const fielder = arr.fielder;
        const bowler = arr.bowler;
        const dismissedBy = fielder || bowler;
        if (accumulator.hasOwnProperty(dismissedPlayer)) {
            if (accumulator[dismissedPlayer].hasOwnProperty(dismissedBy)) {
                accumulator[dismissedPlayer][dismissedBy]++;
            } else {
                accumulator[dismissedPlayer][dismissedBy] = 1;
            }
        } else {
            accumulator[dismissedPlayer] = {};
            accumulator[dismissedPlayer][dismissedBy] = 1;
        }
        return accumulator;
    }, {});

    /** Calculates and updates result object with player(s) who dismissed maximum number of time for each batsmen */
    maxForEachBatsman(result);

    return result;
}

/**
 * Calculates and updates result object with player(s) who dismissed maximum number of time for each batsmen
 * @param {object} result 
 * @returns {void}
 */
function maxForEachBatsman(result) {

    /** Loops each object of object and finds the player(s) who dismissed maximum number of times */
    for (const key in result) {
        let dismissedBy = {};
        let noOfTimes = 0;
        for (const key1 in result[key]) {
            const tempNoOfTimes = result[key][key1];
            if (tempNoOfTimes > noOfTimes) {
                noOfTimes = tempNoOfTimes;
                dismissedBy = {
                    [key1]: noOfTimes
                };
            } else if (noOfTimes == tempNoOfTimes) {
                dismissedBy[key1] = noOfTimes;
            }
        }
        result[key] = dismissedBy;
    }
}

module.exports = highestNumberOfDismissals;