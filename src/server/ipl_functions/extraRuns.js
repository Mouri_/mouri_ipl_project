/**
 * Processes the matches and deliveries array to find the extra runs conceded per team in the year 2016
 * @param {Array} matchesArray 
 * @param {Array} deliveriesArray 
 * @param {number} extraRunsYear
 * @returns {Object} 
 */
function extraRuns({
    matchesArray,
    deliveriesArray,
    extraRunsYear = 2016
}) {

    /** Gets the ID range */
    const fromAndToID = findMatchID(extraRunsYear, matchesArray);

    /** Extracting values from fromAndToID object */
    const fromID = fromAndToID.fromID;
    const toID = fromAndToID.toID;

    /** Loops through deliveries array and updates the accumulator (object) with extra runs each team conceeds and returns it */
    return deliveriesArray.reduce((accumulator, arr) => {
        const ID = arr.match_id;
        if (ID >= fromID && ID <= toID) {
            let team = arr.bowling_team;
            let runs = arr.extra_runs;
            if (accumulator.hasOwnProperty(team)) {
                accumulator[team] += parseInt(runs);
            } else {
                accumulator[team] = parseInt(runs);
            }
        }
        return accumulator;
    }, {});
}

/**
 * Takes in a year and finds its ID range and returns it
 * @param {number} matchIdYear 
 * @param {Array} matchesArray
 * @returns {object}
 */
function findMatchID(matchIdYear, matchesArray) {

    let fromID = Number.MAX_VALUE;
    let toID = 0;

    /** Loops through matches array to find the ID range */
    matchesArray.forEach(arr => {
        let ID = parseInt(arr.id);
        let year = parseInt(arr.season);
        if (year == matchIdYear) {
            fromID = Math.min(fromID, ID);
            toID = Math.max(toID, ID);
        }
    });

    return {
        fromID: fromID,
        toID: toID,
    };
}

module.exports = extraRuns;