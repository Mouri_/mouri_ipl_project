/**
 * Processes the matches array to find the number of times each team won the toss and also won the match
 * @param {Array} matchesArray
 * @returns {Object}
 */
function wonTheTossAndWonTheMatch(matchesArray) {

    /**
     * Loops through matches array and updates the accumulator (object) with the number of times a team won both the toss and the match
     * @constant
     * @type {object}
     */
    return matchesArray.reduce((accumulator, arr) => {
        const matchWinner = arr.winner;
        if (arr.toss_winner == matchWinner) {
            if (accumulator.hasOwnProperty(matchWinner)) {
                accumulator[matchWinner]++;
            } else {
                accumulator[matchWinner] = 1;
            }
        }
        return accumulator;
    }, {});
}

module.exports = wonTheTossAndWonTheMatch;