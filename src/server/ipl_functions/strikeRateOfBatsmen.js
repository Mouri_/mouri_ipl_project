/**
 * Processes the matches array to find the strike rate of a batsman for each season
 * @param {Array} matchesArray 
 * @param {Array} deliveriesArray 
 * @param {string} playerName 
 * @returns {Object}
 */
function strikeRateOfBatsmen({
    matchesArray,
    deliveriesArray,
    playerName
}) {

    /**
     * Loops through deliveries array and updates the accumulator (object) with total runs and number of balls of each batsman per season
     * @constant
     * @type {object}
     */
    let strikeRate = deliveriesArray.reduce((accumulator, arr) => {
        const name = arr.batsman;
        const runs = parseInt(arr.total_runs);
        const ID = arr.match_id;
        const year = findYearFromID(ID, matchesArray);
        if (accumulator.hasOwnProperty(name)) {
            if (accumulator[name].hasOwnProperty(year)) {
                accumulator[name][year].runs += runs;
                accumulator[name][year].balls++;
            } else {
                accumulator[name][year] = {};
                accumulator[name][year].runs = runs;
                accumulator[name][year].balls = 1;
            }
        } else {
            accumulator[name] = {};
            accumulator[name][year] = {};
            accumulator[name][year].runs = runs;
            accumulator[name][year].balls = 1;
        }
        return accumulator;
    }, {});

    /** Calculates and updates the strikeRate object with strike rate */
    for (const key in strikeRate) {
        for (const key1 in strikeRate[key]) {
            strikeRate[key][key1] = ((strikeRate[key][key1].runs / strikeRate[key][key1].balls) * 100).toFixed(2);
        }
    }

    /** Checks if any specific name was given as a parameter to provide result for that particular player */
    if (strikeRate.hasOwnProperty(playerName)) {
        strikeRate = {
            [playerName]: strikeRate[playerName],
        };
    }

    return strikeRate;
}

/**
 * Importing findYearFromID function to find the year of an ID
 * @param {string} ID 
 * @param {Array} matchesArray
 * @param {Array} matchesArrayHeader
 * @returns {string}
 */
function findYearFromID(ID, matchesArray) {

    /** Loops through matches array to find the year for given ID */
    for (const arr of matchesArray) {
        if (ID == arr.id) {
            return arr.season;
        }
    }

}

module.exports = strikeRateOfBatsmen;