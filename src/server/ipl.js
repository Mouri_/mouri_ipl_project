/**
 * Importing path module to convert relative path to absolute path
 * @constant
 * @type {function}
 */
const path = require("path");

const ABSOLUTE_PATH_OF_IPL_FUNCTIONS = path.resolve('./ipl_functions');

/** Imports all the needed functions from ipl_functions */
const matchesPerYear = require(ABSOLUTE_PATH_OF_IPL_FUNCTIONS + '/matchesPerYear');
const matchesWonPerYear = require(ABSOLUTE_PATH_OF_IPL_FUNCTIONS + '/matchesWonPerYear');
const extraRuns = require(ABSOLUTE_PATH_OF_IPL_FUNCTIONS + '/extraRuns');
const topEconomicalBowlers = require(ABSOLUTE_PATH_OF_IPL_FUNCTIONS + '/topEconomicalBowlers');
const wonTheTossAndWonTheMatch = require(ABSOLUTE_PATH_OF_IPL_FUNCTIONS + '/wonTheTossAndWonTheMatch');
const highestNumberOfPlayerOfTheMatch = require(ABSOLUTE_PATH_OF_IPL_FUNCTIONS + '/highestNumberOfPlayerOfTheMatch');
const strikeRateOfBatsmen = require(ABSOLUTE_PATH_OF_IPL_FUNCTIONS + '/strikeRateOfBatsmen');
const highestNumberOfDismissals = require(ABSOLUTE_PATH_OF_IPL_FUNCTIONS + '/highestNumberOfDismissals');
const bestSuperOverEconomyBowler = require(ABSOLUTE_PATH_OF_IPL_FUNCTIONS + '/bestSuperOverEconomyBowler');

/**
 * Exports all the needed functions to be used on index.js
 */
module.exports = {
    matchesPerYear,
    matchesWonPerYear,
    extraRuns,
    topEconomicalBowlers,
    wonTheTossAndWonTheMatch,
    highestNumberOfPlayerOfTheMatch,
    strikeRateOfBatsmen,
    highestNumberOfDismissals,
    bestSuperOverEconomyBowler,
};